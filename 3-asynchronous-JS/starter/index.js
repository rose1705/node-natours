const fs = require("fs");
const superagent = require("superagent");

// fs.readFile(`${__dirname}/dog.txt`, (err, data) => {
//   console.log(`Breed: ${data}`);

//   superagent
//     .get(`https://dog.ceo/api/breed/${data}/images/random`)
//     .end((err, res) => {
//       if (err) return console.log(err.message);
//       console.log(res.body);

//       fs.writeFile("dog-image.txt", res.body.message, err => {
//         if (err) return console.log(err.message);
//         console.log("Random dog image saved to file.");
//       });
//     });
// });

const readFilePro = file => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, (err, data) => {
      if (err) reject("Failed to read file 🙁");
      resolve(data);
    });
  });
};

const writeFilePro = (file, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, err => {
      if (err) reject("Failed to write data to file 😭");

      resolve("File written successfully! 🥳");
    });
  });
};

const getDogPicture = async () => {
  try {
    const data = await readFilePro(`${__dirname}/dog.txt`);
    console.log(`Breed: ${data}`);

    const res = await superagent.get(
      `https://dog.ceo/api/breed/${data}/images/random`
    );
    console.log(res.body.message);

    await writeFilePro("dog-Image.txt", res.body.message);
    console.log("Random dog image saved 🐶");
  } catch (err) {
    console.log(err);
  }

  return "2: Done!";
};

(async () => {
  try {
    console.log("1: Will get dog pictures!");
    const x = await getDogPicture();
    console.log(x);
    console.log("3: Ready! 🤩");
  } catch (err) {
    console.log("ERROR 💥");
  }
})();

// readFilePro(`${__dirname}/dog.txt`)
//   .then(data => {
//     console.log(`Breed: ${data}`);
//     return superagent.get(`https://dog.ceo/api/breed/${data}/images/random`);
//   })
//   .then(res => {
//     writeFilePro("dog-Image.txt", res.body.message);
//   })
//   .then(() => {
//     console.log("Random dog image saved 🐶");
//   })
//   .catch(err => console.log(err));
